import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CourtComponent } from './court/court.component';
import { BallComponent } from './ball/ball.component';
import { MyPaddleComponent } from './my-paddle/my-paddle.component';
import { LeftPaddleComponent } from './left-paddle/left-paddle.component';
import { RightPaddleComponent } from './right-paddle/right-paddle.component';
import { TopPaddleComponent } from './top-paddle/top-paddle.component';
import { PreGameComponent } from './pre-game/pre-game.component';

@NgModule({
  declarations: [
    AppComponent,
    CourtComponent,
    BallComponent,
    MyPaddleComponent,
    LeftPaddleComponent,
    RightPaddleComponent,
    TopPaddleComponent,
    PreGameComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
