import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-right-paddle',
  templateUrl: './right-paddle.component.html',
  styleUrls: ['./right-paddle.component.scss']
})
export class RightPaddleComponent implements OnInit {

  @Input() position: number;
  @Input() size: number;

  constructor() { }

  ngOnInit() {
  }

  get positionPercentage(): string {
    return (this.position - this.size / 2) * 100 + '%';
  }

  get sizePercentage(): string {
    return this.size * 100 + '%';
  }
}
