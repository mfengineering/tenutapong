import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-my-paddle',
  templateUrl: './my-paddle.component.html',
  styleUrls: ['./my-paddle.component.scss']
})
export class MyPaddleComponent implements OnInit {

  @Input() position: number;
  @Input() size: number;

  constructor() { }

  ngOnInit() {
  }

  get positionPercentage(): string {
    return (this.position - this.size / 2) * 100 + '%';
  }

  get sizePercentage(): string {
    return this.size * 100 + '%';
  }

}
