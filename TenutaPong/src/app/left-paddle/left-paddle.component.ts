import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-left-paddle',
  templateUrl: './left-paddle.component.html',
  styleUrls: ['./left-paddle.component.scss']
})
export class LeftPaddleComponent implements OnInit {

  @Input() position: number;
  @Input() size: number;

  constructor() { }

  ngOnInit() {
  }

  get positionPercentage(): string {
    return (this.position - this.size / 2) * 100 + '%';
  }

  get sizePercentage(): string {
    return this.size * 100 + '%';
  }

}
