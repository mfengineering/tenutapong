import {Component, HostListener, OnInit} from '@angular/core';

import {SessionService} from '../session/session.service';

@Component({
  selector: 'app-court',
  templateUrl: './court.component.html',
  styleUrls: ['./court.component.scss']
})
export class CourtComponent implements OnInit {

  private windowsWidth: number;

  constructor(public sessionService: SessionService) { }

  ngOnInit() {
    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.windowsWidth = window.innerWidth;
  }

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(e: MouseEvent) {
    this.sessionService.updateMyPosition(e.clientX / this.windowsWidth);
  }

 @HostListener('window:deviceorientation', ['$event'])
  onOrientation(e: DeviceOrientationEvent) {
    // Pu -> Portrait (up)    180 degree: 150  to 180  to -150  degree
    // Ll -> Landscape Left   -90 degree: -60  to -90  to -120  degree
    // Lr -> Landscape Right   90 degree:  60  to  90  to  120  degree
    // Pn -> Portrait (normal)  0 degree: -30  to   0  to   30  degree

    var x;

    if ((e.gamma >= 150) && (e.gamma <= 180)) {
      x = e.gamma - 150;
    } else if ((e.gamma >= -180) && (e.gamma <= -150)) {
      x = e.gamma + 150;
    } else if ((e.gamma <= -60) && (e.gamma >= -120)) {
      x = e.gamma + 120;
    } else if ((e.gamma >= 60) && (e.gamma <= 120)) {
      x = e.gamma - 60;
    } else if ((e.gamma >= -30) && (e.gamma <= 0)) {
      x = e.gamma + 30;
    } else if ((e.gamma <= 30) && (e.gamma >= 0)) {
      x = e.gamma + 30;
    } else {
      return;
    }

    x = Math.max(0, x);
    x = Math.min(60, x);
    this.sessionService.updateMyPosition( (x / 60));
  }

}
