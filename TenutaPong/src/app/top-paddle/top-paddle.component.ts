import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-top-paddle',
  templateUrl: './top-paddle.component.html',
  styleUrls: ['./top-paddle.component.scss']
})
export class TopPaddleComponent implements OnInit {

  @Input() position: number;
  @Input() size: number;

  constructor() { }

  ngOnInit() {
  }

  get positionPercentage(): string {
    return (this.position - this.size / 2) * 100 + '%';
  }

  get sizePercentage(): string {
    return this.size * 100 + '%';
  }
}
