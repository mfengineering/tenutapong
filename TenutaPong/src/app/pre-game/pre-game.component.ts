import {Component, Input, OnInit} from '@angular/core';
import {SessionService} from '../session/session.service';

@Component({
  selector: 'app-pre-game',
  templateUrl: './pre-game.component.html',
  styleUrls: ['./pre-game.component.scss']
})
export class PreGameComponent implements OnInit {

  @Input() numberOfPlayers: number;

  constructor(private sessionService: SessionService) { }

  ngOnInit() {
  }

  startGame() {
    this.sessionService.startGame();
  }
}
