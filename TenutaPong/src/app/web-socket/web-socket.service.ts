import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

import {Model} from './model';
import {PaddlePosition} from './paddle-position';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private ws: WebSocket;
  private modelSubject = new Subject<Model>();
  model$ = this.modelSubject.asObservable();

  constructor() {
    this.ws = new WebSocket(WebSocketService.address);

    this.ws.onmessage = (e) => this.modelSubject.next(JSON.parse(e.data) as Model);
  }

  updatePaddlePosition(data: PaddlePosition) {
    this.ws.send(JSON.stringify({ command: 'updatePaddlePosition', data: data}));
  }

  updateBallVelocity(velocityRatioX: number, velocityRatioY: number) {
    this.ws.send(JSON.stringify({ command: 'updateBallVelocity', velocityRatioX: velocityRatioX, velocityRatioY: velocityRatioY}));
  }

  startGame() {
    this.ws.send(JSON.stringify({ command: 'startGame'}));
  }


  private static get address(): string {
    const url = window.location.host;

    if (url.indexOf(':4200') > 0) {
      // development mode with 'ng serve'
      // in this case the server is usually also started locally on port 3000
      return 'ws://localhost:3000';
    }
    return 'ws://' + url;
  }
}
