import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ball',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.scss']
})
export class BallComponent implements OnInit {

  @Input() private positionX: number;
  @Input() private positionY: number;
  @Input() private size: number;

  constructor() {
  }

  ngOnInit() {
  }

  get leftPercentage(): string {
    return (this.positionX - this.size / 2) * 100 + '%';
  }

  get topPercentage(): string {
    return (this.positionY - this.size / 2) * 100 + '%';
  }

  get sizePercentage(): string {
    return this.size * 100 + '%';
  }
}
