import {Player} from './player';
import {Ball} from './ball';

export class Session {
  players: Player[] = [];
  ball = new Ball();
  running = false;
}
