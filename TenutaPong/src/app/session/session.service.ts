import { Injectable } from '@angular/core';
import {WebSocketService} from '../web-socket/web-socket.service';
import {interval, Subject} from 'rxjs';
import {Session} from './session';
import {Ball} from './ball';
import {Player} from './player';
import {Model} from '../web-socket/model';
import {distinctUntilChanged, filter, throttleTime} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  updateRate = 5;
  myIndex = 0;
  private session = new Session();
  private myPositionSubject = new Subject<number>();

  ball = new Ball();
  myPlayer = new Player();

  audioCollision = new Audio('assets/collision.mp3');

  constructor(private webSocketService: WebSocketService) {
    this.webSocketService.model$.subscribe(m => this.update(m));

    interval(this.updateRate)
      .subscribe(() => this.animate());

    this.myPositionSubject.pipe(
      filter(() => this.mySessionPlayer != null),
      throttleTime(40),
      distinctUntilChanged(),
    ).subscribe(v => this.internalUpdateMyPosition(v));
  }

  updateMyPosition(value: number) {
    value = Math.min(value, 1 - this.myPlayer.paddleSize / 2);
    value = Math.max(value, this.myPlayer.paddleSize / 2);
    value = Math.round(value * 1000) / 1000;
    this.myPositionSubject.next(value);
  }

  get rightPlayer(): Player {
    return this.session.players[(this.myIndex + 1) % 4];
  }

  get topPlayer(): Player {
    return this.session.players[(this.myIndex + 2) % 4];
  }

  get leftPlayer(): Player {
    return this.session.players[(this.myIndex + 3) % 4];
  }

  get sessionRunning(): boolean {
    return this.session.running;
  }

  get numberOfPlayers(): number {
    return this.session.players.length;
  }

  startGame() {
    this.webSocketService.startGame();
  }

  private internalUpdateMyPosition(value: number) {
    try {
      this.myPlayer.paddlePosition = value;
      this.webSocketService.updatePaddlePosition({index: this.myIndex, position: value});
    } catch (e) {
      console.log(e);
    }
  }

  private get mySessionPlayer(): Player {
    return this.session.players[this.myIndex];
  }

  private update(model: Model) {
    this.myIndex = model.index;
    this.session = model.session as Session;
    this.ball.updateFromGlobal(model.session.ball, this.myIndex);

    if (this.mySessionPlayer) {
      this.myPlayer.paddleSize = this.mySessionPlayer.paddleSize;
    }
  }

  private animate() {
    if (!this.session.running) {
      return;
    }

    this.ball.positionX += this.ball.velocityRatioX / (this.ball.courtCrossTime * 1000 / this.updateRate);
    this.ball.positionY += this.ball.velocityRatioY / (this.ball.courtCrossTime * 1000 / this.updateRate);

    if (this.ball.velocityRatioY < 0) {
      return;
    }

    if (this.ball.positionY + this.ball.size / 2 < 0.97) {
      return;
    }

    if (this.ball.positionX < this.myPlayer.paddlePosition - this.myPlayer.paddleSize / 2) {
      return;
    }

    if (this.ball.positionX > this.myPlayer.paddlePosition + this.myPlayer.paddleSize / 2) {
      return;
    }

    const velocityRatio = Math.sqrt(Math.pow(this.ball.velocityRatioX, 2) + Math.pow(this.ball.velocityRatioY, 2));

    const offsetX = (this.ball.positionX - this.myPlayer.paddlePosition) * 10;
    this.ball.velocityRatioX += offsetX;
    this.ball.velocityRatioY = -Math.sqrt(Math.pow(velocityRatio, 2) - Math.pow(this.ball.velocityRatioX, 2));

    // noinspection JSIgnoredPromiseFromCall
    this.audioCollision.play();
    const globalBall = this.ball.convertToGlobal(this.myIndex);
    this.webSocketService.updateBallVelocity(globalBall.velocityRatioX, globalBall.velocityRatioY);
  }
}
