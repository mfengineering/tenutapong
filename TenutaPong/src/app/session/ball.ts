export class Ball {
  positionXShadow = 0.5;
  positionYShadow = 0.5;
  velocityRatioX = 0.4;
  velocityRatioY = 0.9;
  courtCrossTime = 1.5;
  size = 0.04;

  get positionX(): number {
    return this.positionXShadow;
  }

  set positionX(value: number) {
    const min = this.size / 2;
    const max = 1 - min;
    this.positionXShadow = Math.max(Math.min(value, max), min);
  }

  get positionY(): number {
    return this.positionYShadow;
  }

  set positionY(value: number) {
    const min = this.size / 2;
    const max = 1 - min;
    this.positionYShadow = Math.max(Math.min(value, max), min);
  }

  updateFromGlobal(ball: Ball, index: number) {
    this.courtCrossTime = ball.courtCrossTime;

    switch (index) {
      case 0:
        this.positionX = ball.positionX;
        this.positionY = ball.positionY;
        this.velocityRatioX = ball.velocityRatioX;
        this.velocityRatioY = ball.velocityRatioY;
        break;

      case 1:
        this.positionX = 1 - ball.positionY;
        this.positionY = ball.positionX;
        this.velocityRatioX = -ball.velocityRatioY;
        this.velocityRatioY = ball.velocityRatioX;
        break;

      case 2:
        this.positionX = 1 - ball.positionX;
        this.positionY = 1 - ball.positionY;
        this.velocityRatioX = -ball.velocityRatioX;
        this.velocityRatioY = -ball.velocityRatioY;
        break;

      case 3:
        this.positionX = ball.positionY;
        this.positionY = 1 - ball.positionX;
        this.velocityRatioX = ball.velocityRatioY;
        this.velocityRatioY = -ball.velocityRatioX;
        break;
    }
  }

  convertToGlobal(index: number): Ball {
    const globalBall = new Ball();

    switch (index) {
      case 0:
        globalBall.positionX = this.positionX;
        globalBall.positionY = this.positionY;
        globalBall.velocityRatioX = this.velocityRatioX;
        globalBall.velocityRatioY = this.velocityRatioY;
        break;

      case 1:
        globalBall.positionX = this.positionY;
        globalBall.positionY = 1 - this.positionX;
        globalBall.velocityRatioX = this.velocityRatioY;
        globalBall.velocityRatioY = -this.velocityRatioX;
        break;

      case 2:
        globalBall.positionX = 1 - this.positionX;
        globalBall.positionY = 1 - this.positionY;
        globalBall.velocityRatioX = -this.velocityRatioX;
        globalBall.velocityRatioY = -this.velocityRatioY;
        break;

      case 3:
        globalBall.positionX = 1 - this.positionY;
        globalBall.positionY = this.positionX;
        globalBall.velocityRatioX = -this.velocityRatioY;
        globalBall.velocityRatioY = this.velocityRatioX;
        break;
    }

    return globalBall;
  }
}
