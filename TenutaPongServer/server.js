// Simple web server and websocket server
// (c) 2018, Reto Bättig
//
// The web server serves static files from the directory "./public"
// the web socket server synchronizes the data structure "model" with all connected clients
// the web socket server expects a json structure and knows 2 commands:
//      {command: "getdata"}
//      {command: "setdata", model: {... arbitrary data model which gets distributed to all clients }}


const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);
const normalizePort = require('normalize-port');

class Player {

    constructor() {
        this.paddlePosition = 0;
        this.paddleSize = 0.15;
    }
}

class Ball {
    constructor() {
        this.positionX = 0.5;
        this.positionY = 0.5;
        this.velocityRatioX = 0.4;
        this.velocityRatioY = 0.9;
        this.courtCrossTime = 1.5;
    }

    animate(intervalMs) {
        const min = 0.02; // ball size/2
        const max = 1 - min;

        this.positionX += this.velocityRatioX / (this.courtCrossTime * 1000 / intervalMs);
        this.positionY += this.velocityRatioY / (this.courtCrossTime * 1000 / intervalMs);

        if (this.positionX >= max) {
            this.positionX = max;
            this.velocityRatioX = -this.velocityRatioX;
        } else if (this.positionX <= min) {
            this.positionX = min;
            this.velocityRatioX = -this.velocityRatioX;
        } else if (this.positionY >= max) {
            this.positionY = max;
            this.velocityRatioY = -this.velocityRatioY;
        } else if (this.positionY <= min) {
            this.positionY = min;
            this.velocityRatioY = -this.velocityRatioY;
        }
    }
}

class SessionModel {
    constructor() {
        this.players = [];
        this.ball = new Ball();
        this.running = false;
    }

    animate(intervalMs) {
        this.ball.animate(intervalMs);
    }
}

class Session {
    constructor(onStarted) {
        console.log('Session.ctor()');
        this.model = new SessionModel();
        this.clients = [];
        this.timer = null;
        this.intervalMs = 50;
        this.onStarted = onStarted;
    }

    add(client) {
        this.model.players.push(new Player());
        this.clients.push(client);

        client.on('message', msg => this.handleMessage(JSON.parse(msg)));
        client.on('close', () => this.remove(client));
        this.updateClients();
    }

    get isFull() {
        return this.model.players.length === 4;
    }

    start() {
        this.onStarted();
        this.model.running = true;
        this.timer = setInterval(() => {
            this.model.animate(this.intervalMs);
            this.updateClients();
        }, this.intervalMs);
    }

    handleMessage(message) {
        if (message.command === 'updatePaddlePosition') {
            this.model.players[message.data.index].paddlePosition = message.data.position;
        } else if (message.command === 'updateBallVelocity') {
            this.model.ball.velocityRatioX = message.velocityRatioX;
            this.model.ball.velocityRatioY = message.velocityRatioY;
        } else if (message.command === 'startGame') {
            this.start();
        } else {
            console.log('Error: unknown command: ' + message.command);
            return;
        }


        this.updateClients();
    }

    remove(client) {
        const index = this.clients.indexOf(client);
        if (index >= 0) {
            this.model.players[index] = null;
            this.clients.splice(index, 1);
        }

        this.updateClients();
    }

    updateClients() {
        this.clients.forEach((c, i) => {
            try {
                c.send(JSON.stringify({index: i, session: this.model}));
            }
            catch (e) {
            }
        });
    }
}

class Broker {

    constructor() {
        this.sessions = [];
        this.createNewSession();
    }

    add(client) {
        console.log('Broker.add()');
        this.nextSession.add(client);
        if(this.nextSession.isFull) {
            this.nextSession.start();
        }
    }

    createNewSession() {
        this.sessions.push(new Session(() => this.createNewSession()));
    }

    get nextSession() {
        return this.sessions[this.sessions.length - 1];
    }
}

const broker = new Broker();
app.ws('/', c => broker.add(c));

// Static Web Server
app.use(express.static('public'));

// Start Web Server
const port = normalizePort(process.env.PORT || '3000');
app.listen(port, function () {
    console.log('Web Server listening on port '+port);
});